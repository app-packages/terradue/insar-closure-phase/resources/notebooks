## Run python File

Clone the GitLab repository to your working directory.

Create an environment with all the required packages, using the command:
```
mamba env create -f environment
```
or
```
conda env create -f environment
```

Activate the environment using the command:
```
conda activate <env_name>
```
and execute:
```
python -m ipykernel install --user --name <env_name> --display-name "<env_name>"
```

Select the kernel just created and run the script.