# General libraries
import os
import json
import sys
import pprint
import copy
import tarfile
from pathlib import Path

# Making requests and monitoring progress
import requests
from time import sleep
import getpass

# Extract results from S3
from urllib.parse import urlparse
import boto3


headers_exec = {'Content-Type': 'application/json',
                'Accept': 'application/json'}

def getResponse(url):

    r = requests.get(
        url,
        headers=headers_exec
    )

    assert r.status_code == 200 and r.reason == 'OK', f'ERROR: request failed: {r.status_code} {r.reason}'
    response = r.json()
    
    return response